import os
from setuptools import setup, find_packages


def read_file(filename):
    """Read a file into a string"""
    path = os.path.abspath(os.path.dirname(__file__))
    filepath = os.path.join(path, filename)
    try:
        return open(filepath).read()
    except IOError:
        return ''


def get_readme():
    """Return the README file contents. Supports text,rst, and markdown"""
    for name in ('README', 'README.rst', 'README.md'):
        if os.path.exists(name):
            return read_file(name)
    return ''


# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

# Use the docstring of the __init__ file to be the description
DESC = " ".join(__import__('portfolio').__doc__.splitlines()).strip()

setup(
    name="wagpress-portfolio",
    version=__import__('portfolio').get_version().replace(' ', '-'),
    packages=find_packages(),
    include_package_data=True,
    license='BSD',
    description=DESC,
    long_description=get_readme(),
    url='https://github.com/lanshark/wagpress-portfolio',
    author='Scott Sharkey',
    author_email='ssharkey@lanshark.com',
    install_requires=read_file('requirements/base.txt'),
    zip_safe=False,
    keywords='Wagtail Portfolio',
    project_urls={
            'Source': 'https://github.com/lanshark/wagpress-portfolio',
            'Tracker': 'https://github.com/lanshark/wagpress-portfolio/issues',
    },
    classifiers=[
        'Environment :: Web Environment',
        'Development Status :: 4 - Beta',
        'Framework :: Django',
        'Framework :: Django :: 1.11',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content'
    ],
)
