# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'p_ymcf16*u)rp+5$owspy$!eu%0hrf1d3-+**gd-_-7p5xx)pw'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INTERNAL_IPS = (
    '127.0.0.1',
)

try:
    from .local import *
except ImportError:
    pass
