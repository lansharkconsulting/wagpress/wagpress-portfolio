import factory


class PortfolioFactory(factory.django.DjangoModelFactory):
    """
    Factory for :model:`portfolio.Portfolio`
    """
    # SAS -- fields need updated...
    address = factory.Faker('address')
    name = factory.Faker('name')
    city = 'New York'
    state = 'New York'
    postal_code = '10001'
    phone_number = '+1-213-621-0002'
    fax_number = '+1-503-254-1000'
    poc = factory.SubFactory(PortfolioCategory)

    class Meta:
        model = 'portfolio.Portfolio'
