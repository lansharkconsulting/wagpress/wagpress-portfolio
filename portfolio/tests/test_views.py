from django.test import TestCase

from faker import Faker

from .factories import (
    PortfolioFactory,
)

fake = Faker()


class TestPortfolio(TestCase):
    """
    Tests the portfolio.
    """

    def setUp(self):
        self.portfolio = PortfolioFactory()

    def test_portfolio(self):
        pass
