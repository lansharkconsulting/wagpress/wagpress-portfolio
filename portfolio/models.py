from __future__ import absolute_import, unicode_literals

from django.contrib import messages
from django.db import models
from django.shortcuts import redirect, render
from django.utils.encoding import python_2_unicode_compatible

from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey

from taggit.models import Tag, TaggedItemBase

from wagtail.contrib.wagtailroutablepage.models import RoutablePageMixin, route
from wagtail.wagtailadmin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailcore.models import Page
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index

from .blocks import BaseStreamBlock


@python_2_unicode_compatible
class PortfolioPageTag(TaggedItemBase):
    """
    This model allows us to create a many-to-many relationship between
    the PortfolioPage object and tags. There's a longer guide on using it at
    http://docs.wagtail.io/en/latest/reference/pages/model_recipes.html#tagging.
    """
    content_object = ParentalKey('PortfolioPage', related_name='tagged_items', on_delete=models.CASCADE)

    class Meta:
        app_label = 'portfolio'


@python_2_unicode_compatible
class PortfolioPage(Page):
    """
    A Portfolio Page

    """
    project = models.TextField(
        help_text='Text to describe the portfolio project',
        blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Landscape mode only; horizontal width between 1000px and 3000px.'
    )
    description = StreamField(
        BaseStreamBlock(), verbose_name="Project description", blank=True
    )
    subtitle = models.CharField(blank=True, max_length=255)
    tags = ClusterTaggableManager(through=PortfolioPageTag, blank=True)
    date_published = models.DateField(
        "Date article published", blank=True, null=True
        )

    content_panels = Page.content_panels + [
        FieldPanel('subtitle', classname="full"),
        FieldPanel('project', classname="full"),
        ImageChooserPanel('image'),
        StreamFieldPanel('description'),
        FieldPanel('date_published'),
        FieldPanel('tags'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('description'),
    ]

    @property
    def get_tags(self):
        """
        We're returning all the tags that are related to the portfolio post into a
        list we can access on the template.  We're additionally adding a URL to
        access PortfolioPage objects with that tag.
        """
        tags = self.tags.all()
        for tag in tags:
            tag.url = '/'+'/'.join(s.strip('/') for s in [
                self.get_parent().url,
                'tags',
                tag.slug
            ])
        return tags

    # Specifies parent to PortfolioPage as being PortfolioIndexPages
    parent_page_types = ['PortfolioIndexPage']

    # Specifies what content types can exist as children of PortfolioPage.
    # Empty list means that no child content types are allowed.
    subpage_types = []

    def __str__(self):
        return "portfolio: {}".format(self.project)

    class Meta:
        app_label = 'portfolio'


@python_2_unicode_compatible
class PortfolioIndexPage(RoutablePageMixin, Page):
    """
    Index page for portfolios.

    We need to alter the page model's context to return the child page objects,
    the PortfolioPage objects, so that it works as an index page.

    RoutablePageMixin is used to allow for a custom sub-URL for the tag views
    defined above.
    """
    project = models.TextField(
        help_text='Text to describe the porfolio project',
        blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Landscape mode only; horizontal width between 1000px and 3000px.'
    )

    content_panels = Page.content_panels + [
        FieldPanel('project', classname="full"),
        ImageChooserPanel('image'),
    ]

    # Specifies that only PortfolioPage objects can live under this index page
    subpage_types = ['PortfolioPage']

    # Defines a method to access the children of the page (e.g. PortfolioPage
    # objects). On the demo site we use this on the HomePage
    def children(self):
        return self.get_children().specific().live()

    # Overrides the context to list all child items, that are live, by the
    # date that they were published
    # http://docs.wagtail.io/en/latest/getting_started/tutorial.html#overriding-context
    def get_context(self, request):
        context = super(PortfolioIndexPage, self).get_context(request)
        context['posts'] = PortfolioPage.objects.descendant_of(
            self).live().order_by(
            '-date_published')
        return context

    # This defines a Custom view that utilizes Tags. This view will return all
    # related PortfolioPages for a given Tag or redirect back to the PortfolioIndexPage.
    # More information on RoutablePages is at
    # http://docs.wagtail.io/en/latest/reference/contrib/routablepage.html
    @route('^tags/$', name='tag_archive')
    @route('^tags/(\w+)/$', name='tag_archive')
    def tag_archive(self, request, tag=None):

        try:
            tag = Tag.objects.get(slug=tag)
        except Tag.DoesNotExist:
            if tag:
                msg = 'There are no portfolio posts tagged with "{}"'.format(tag)
                messages.add_message(request, messages.INFO, msg)
            return redirect(self.url)

        posts = self.get_posts(tag=tag)
        context = {
            'tag': tag,
            'posts': posts
        }
        return render(request, 'portfolio/portfolio_index_page.html', context)

    def serve_preview(self, request, mode_name):
        # Needed for previews to work
        return self.serve(request)

    # Returns the child PortfolioPage objects for this PortfolioPageIndex.
    # If a tag is used then it will filter the posts by tag.
    def get_posts(self, tag=None):
        posts = PortfolioPage.objects.live().descendant_of(self)
        if tag:
            posts = posts.filter(tags=tag)
        return posts

    # Returns the list of Tags for all child posts of this PortfolioPage.
    def get_child_tags(self):
        tags = []
        for post in self.get_posts():
            # Not tags.append() because we don't want a list of lists
            tags += post.get_tags
        tags = sorted(set(tags))
        return tags

    def __str__(self):
        return "portfolioIndex: {}".format(self.project)

    class Meta:
        app_label = 'portfolio'
