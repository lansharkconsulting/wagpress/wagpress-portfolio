# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-25 18:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectcategorymetafielddefaultkeys',
            name='category',
        ),
        migrations.RemoveField(
            model_name='projectcategorymetafielddefaultkeys',
            name='key',
        ),
        migrations.RemoveField(
            model_name='projectmetafield',
            name='value',
        ),
        migrations.DeleteModel(
            name='ProjectCategoryMetaFieldDefaultKeys',
        ),
    ]
