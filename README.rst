=========
Portfolio
=========

NOTE: this is being developed as part of the `WagPress Project <https://gitlab.com/lansharkconsulting/wagpress>`_.

Portfolio is a simple Django app to show off your Web-based portfolio.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "portfolio" to your INSTALLED_APPS setting like this::

     INSTALLED_APPS = [
         ...
         'portfolio',
     ]

2. Include the portfolio URLconf in your project urls.py like this::

    path('portfolio/', include('portfolio.urls')),

3. Run `python manage.py migrate` to create the portfolio models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a portfolio (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/portfolio/ to see your new portfolio.
