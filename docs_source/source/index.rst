.. wagpress-portfolio documentation master file, created by
   sphinx-quickstart on Mon Feb 26 00:08:53 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to wagpress-portfolio's documentation!
==============================================

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   getting_started
   reference/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
