
Installation
============

Installation is easy using ``pip`` or ``easy_install``.

.. code-block:: bash

	pip install wagpress-portfolio

or

.. code-block:: bash

	easy_install wagpress-portfolio

Dependencies
************
